#include "competitive.h"
#include "linked_list.h"

template<typename T>
void partition(cmp::linked_list<T> list, T pivot) {
    cmp::linked_list<T> left, right;

    auto next = list.head;
    while (next != nullptr) {
        auto data = next->data;
        if (data <=pivot) {
            left.append(data);
        } else if (data > pivot) {
            right.append(data);
        } else {
            break;
        }     

        next = next->next;
    }

    left.append(pivot);

    next = right.head;
    while (next != nullptr) {
        auto data = next->data;
        left.append(data);

        next = next->next;
    }

    left.print();
} 

int main(int argc, char ** argv) {
    std::vector<int> integers { 8, 6, 5, 3, 3, 2, 1, 4, 7, 20, -1, -3 };
    cmp::linked_list<int> before;
    for (auto i : integers) {
        before.append(i);
    };

    before.print();
    partition(before, 3);
    return 0;
}
