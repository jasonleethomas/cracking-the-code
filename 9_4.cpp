#include "competitive.h"

template<typename Set, typename Key>
bool in_set(Set set, Key key) {
    return set.find(key) != std::end(set);
}

void print_set(std::set<int> set) {
    std::copy(std::begin(set), std::end(set),
              std::ostream_iterator<int>(std::cout, " "));
    std::cout << std::endl;
};

std::set<int> erase_copy(std::set<int> set, int key) {
    set.erase(key);

    return set;
}

std::set<std::set<int>> all_subsets(std::set<int> set) {
    static std::set<std::set<int>> cache;

    if (set.empty()) { return std::set<std::set<int>> {}; }

    std::set<std::set<int>> sets_minus_one;
    std::transform(std::begin(set), std::end(set), 
                   std::inserter(sets_minus_one, std::begin(sets_minus_one)),
                   [set, erase_copy](auto item) { return erase_copy(set, item); });
    
    std::set<std::set<int>> sets_minus_cache;
    std::set_difference(std::begin(sets_minus_one), std::end(sets_minus_one),
                        std::begin(cache), std::end(cache),
                        std::inserter(sets_minus_cache, std::begin(sets_minus_cache)));

    std::copy(std::begin(sets_minus_cache), std::end(sets_minus_cache),
              std::inserter(cache, std::begin(cache)));

    auto subsets { sets_minus_cache };

    std::for_each(std::begin(sets_minus_cache), std::end(sets_minus_cache),
                  [&subsets](auto set) {
                        auto subset = all_subsets(set);
                        std::copy(std::begin(subset), std::end(subset),
                                  std::inserter(subsets, std::begin(subsets)));
                  });

    return subsets;
}

int main() {
    std::set<std::set<int>> subsets;
    std::set<int> set {1, 2, 3, 4, 5, 6, 7, 8, 9};

    subsets = all_subsets(set);
    std::for_each(std::begin(subsets), std::end(subsets), print_set);

    return 0;
}

