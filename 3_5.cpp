#include "competitive.h"

template<typename T>
class MyQueue {
public:
    void push(T data) {
        transfer(one, two);
        two.push(data);
        transfer(two, one);
    }

    T pop() {
        assert(!one.empty());

        T top { one.top() };
        one.pop();
        return top;
    }

    bool empty() {
        return one.empty();
    }

    size_t size() {
        return one.size();
    }

private:
    void transfer(std::stack<T> & a, std::stack<T> & b) {
        while (!a.empty()) {
            b.push(a.top());
            a.pop();
        }
    }

    std::stack<T> one, 
                  two;
};

int main() {
    int const data_size = 100;
    std::vector<int> data { data_size };
    std::iota(std::begin(data), std::end(data), 0);

    MyQueue<int> queue;

    std::for_each(std::begin(data), std::end(data), 
                  [&queue](auto item) {
                        queue.push(item);
                  });

    assert(queue.size() == data.size());

    std::for_each(std::rbegin(data), std::rend(data),
                  [&queue](auto item) {
                        auto top = queue.pop();
                        assert(top == item);
                  });
    return 0;
}
