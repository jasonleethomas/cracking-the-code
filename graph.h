#include <algorithm>
#include <cassert>
#include <map>
#include <memory>
#include <numeric>
#include <queue>
#include <random>
#include <set>
#include <type_traits>
#include <vector>

#include <iostream>

namespace cmp {
    template<typename I, typename T>
    struct graph_node {
        graph_node(I index, T data) :
            index(index),
            data(data)
        {}

        void append(std::unique_ptr<graph_node<I, T>> & neighbor) {
            neighbors.push_back(neighbor);
        }

        I index;
        T data;
        std::vector<std::unique_ptr<graph_node<I, T>>> neighbors;
    };

    template<typename I>
    using adjacency_matrix = std::map<I, std::map<I, int>>;

    template<typename I>
    using adjacency_list = std::map<I, std::vector<std::pair<I, int>>>;

    template<typename Node, typename UnaryOperation, typename UnaryPredicate>
    void dfs_from(Node * node, 
                  UnaryOperation const & visit, 
                  UnaryPredicate const & visited) {
        visit(node);
        for (auto & neighbor : node->neighbors) {
            if (!visited(neighbor->index)) {
                dfs_from(neighbor.get(), visit, visited);
            }
        }
    }

    template<typename I, typename UnaryOperation, typename UnaryPredicate>
    void dfs_from(std::pair<I, int> node,
                  adjacency_list<I> & adjacencies, 
                  UnaryOperation const & visit,
                  UnaryPredicate const & visited) {
        visit(node);
        for (auto & neighbor : adjacencies[node.first]) {
            if (!visited(neighbor)) {
                dfs_from(neighbor, adjacencies, visit, visited);
            }
        }
    }

    template<typename Node, typename UnaryOperation, typename UnaryPredicate>
    void bfs_from(Node * node, 
                  UnaryOperation const & visit, 
                  UnaryPredicate const & visited) {
        visit(node);
        std::queue<Node *> queue;
        queue.push(node);
        while (!queue.empty()) {
            auto next = queue.front(); queue.pop();
            for (auto & neighbor : next->neighbors) {
                if (!visited(neighbor.get())) {
                    visit(neighbor.get());
                    queue.push(neighbor.get());
                }
            }
        }
    }

    template<typename I, typename UnaryOperation, typename UnaryPredicate>
    void bfs_from(std::pair<I, int> node, 
                  adjacency_list<I> & adjacencies,
                  UnaryOperation const & visit, 
                  UnaryPredicate const & visited) {

        visit(node);
        std::queue<std::pair<I, int>> queue;
        queue.push(node);
        while (!queue.empty()) {
            auto next = queue.front(); queue.pop();
            for (auto & neighbor : adjacencies[next.first]) {
                if (!visited(neighbor)) {
                    visit(neighbor);
                    queue.push(neighbor);
                }
            }
        }
    }

    template<typename I>
    std::pair<std::map<I, int>, std::map<I, I>>
    dijkstra_shortest_path(I source, std::map<I, std::vector<std::pair<I, int>>> & adjacency_list) {

        std::map<I, I> parent_vertex;
        std::map<I, int> shortest_distance;
        std::transform(std::begin(adjacency_list), std::end(adjacency_list),
                       std::inserter(shortest_distance, std::end(shortest_distance)),
                       [](auto adjacent) { 
                            return std::make_pair(adjacent.first, std::numeric_limits<int>::max()); 
                       });

        shortest_distance[source] = 0;

        std::set<std::pair<int, I>> vertex_heap;
        vertex_heap.insert(std::make_pair(shortest_distance[source], source));

        while (!vertex_heap.empty()) {
            auto u_ptr = std::begin(vertex_heap);
            auto dist_u = u_ptr->first;
            auto u = u_ptr->second;

            vertex_heap.erase(u_ptr);

            for (auto adjacent : adjacency_list[u]) {
                auto v = adjacent.first;
                auto weight_u_v = adjacent.second; 
                auto dist_u_v = dist_u + weight_u_v;

                if (dist_u_v < shortest_distance[v]) {
                    vertex_heap.erase(std::make_pair(shortest_distance[v], v));
                    vertex_heap.insert(std::make_pair(dist_u_v, v));

                    shortest_distance[v] = dist_u_v;
                    parent_vertex[v] = u;
                }
            }
        }

        return std::make_pair(shortest_distance, parent_vertex);
    }
}

