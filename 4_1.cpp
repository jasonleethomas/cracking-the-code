#include "tree.h"
#include "competitive.h"

template<typename Node>
std::pair<bool, int> balanced_from(Node * node) {
    auto balanced = true;
    auto height = -1;

    if (!node) {
        return std::make_pair(balanced, height);
    }

    auto left_balanced = true, right_balanced = true;
    auto left_height = 0, right_height = 0;

    std::tie(left_balanced, left_height) = balanced_from(node->left.get());
    std::tie(right_balanced, right_height) = balanced_from(node->right.get());

    balanced = left_balanced && right_balanced &&
               std::abs(left_height - right_height) < 2;

    height = 1 + std::max(left_height, right_height);

    return std::make_pair(balanced, height);
}

template<typename Node>
std::pair<bool, int> balanced(Node & node) {
    return balanced_from(node.get());
}

int main() {
    std::mt19937 mt { std::random_device{}() };
    std::uniform_int_distribution<int> random_data { -10, 10 };

    auto generator = [](auto node_data) { 
        std::mt19937 mt { std::random_device{}() };
        std::uniform_int_distribution<int> left_data { node_data - 10, node_data };
        std::uniform_int_distribution<int> right_data { node_data, node_data + 10 };

        return std::make_pair(left_data(mt), right_data(mt)); 
    };

    auto binary_tree_root = std::make_unique<cmp::binary_tree_node<int>>(random_data(mt));
    cmp::fill_from(binary_tree_root, generator);

    auto height = 0; 
    auto is_balanced = false;

    std::tie(is_balanced, height) = balanced(binary_tree_root);
    assert(height == height_from(binary_tree_root.get()));

    std::vector<int> generated_data;
    cmp::dfs_from(binary_tree_root, [&](auto node) {
                generated_data.push_back(node->data);
            });

    cmp::print_from(binary_tree_root);
    std::cout << "\n\n"; 

    auto back = generated_data.back(); generated_data.pop_back();
    auto avl_tree_root = std::make_unique<cmp::binary_tree_node<int>>(back);

    std::for_each(std::begin(generated_data), std::end(generated_data),
                  [&](auto data) { avl_tree_root = cmp::append_avl(avl_tree_root, data); });

    cmp::print_from(avl_tree_root);
    std::cout << "b-tree height: " << height << " " 
              << "avl height: " << height_from(avl_tree_root.get()) 
              << "\n"; 

    assert(balanced(avl_tree_root).first);

    return 0;
}
