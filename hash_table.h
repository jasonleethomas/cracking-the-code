#include <algorithm>
#include <array>
#include <functional>
#include <memory>
#include <vector>

template<typename T, typename V, int N>
class chained_hash_table {
public:
    chained_hash_table(std::function<int(T)> hash) :
        hash(hash)
    {}

    void insert(T key, V value) {
        auto const index = hash(key) % table.size();
        table[index].push_back(std::make_pair(key, value));
    }

    std::pair<T, V> const * query(T key) {
        auto const index = hash(key) % table.size();
        auto it = std::find(std::begin(table[index]), std::end(table[index]));
        return it == std::end(table[index])? nullptr : it;
    }

    void remove(T key) {
        auto const index = hash(key) % table.size(); 
        table.erase(std::remove_if(std::begin(table[index]), std::end(table[index]),
                                   [key](auto pair) { return pair.first == key; }));
    }

private:
    std::function<int(T)> const hash;
    std::array<std::vector<std::pair<T, V>>, N> table;
};

template<typename T, typename V>
class open_hash_table {
public:
    enum Resolution { LinearProbe, QuadraticProbe, DoubleHash };
    open_hash_table(int const size, 
                    Resolution resolution, 
                    std::function<int(T)> hash,
                    std::function<int(T)> double_hash = 
                    std::function<int(T)>{}) :
        hash(hash),
        double_hash(double_hash),
        resolution(resolution),
        table(std::max(size, 1), nullptr)
    {
        if (resolution == DoubleHash) { 
            static_assert(double_hash, "No double hash function provided"); 
        }
    }

    int find_index(T key) {
        auto hashed = hash(key);
        auto index = hashed % table.size();
        for (auto k = 0; k < table.size(); ++k) {
            switch(resolution) {
            case LinearProbe:
                index = (hashed + k) % table.size();
                break;
            case QuadraticProbe:
                index = (hashed + k * k) % table.size();
                break;
            case DoubleHash: 
                index = (hashed + k * double_hash(key)) % table.size();
                break;
            }

            if (!table[index])
                break;
        }

        return index;
    }

    void insert(T key, V value) {
        auto index = find_index(key);
        if (table[index]) {
            table[index]->second = value;
            return;
        }

        table[index] = std::make_unique<std::pair<T, V>>(key, value); 
    }

    std::pair<T, V> const * query(T key) {
        auto index = find_index(key); 
        return table[index].get();
    }

    void remove(T key) {
        auto index = find_index(key);
        table[index].reset();
    }

private:
    Resolution const resolution;
    std::function<int(T)> const hash;
    std::function<int(T)> const double_hash;
    std::vector<std::unique_ptr<std::pair<T, V>>> table;
};
