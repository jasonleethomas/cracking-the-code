#include "tree.h"
#include "competitive.h"

template<typename Node>
bool is_subtree (Node a, Node b) {
    if (b == nullptr) {
        return true;
    }

    if (a == nullptr) {
        return false;
    }

    if (a == b) {
        return true && is_subtree(a->left(), b->left()) 
                    && is_subtree(a->right(), b->right());
    }

    return false || is_subtree(a->left(), b) 
                 || is_subtree(a->right(), b);
}

int main() {
    cmp::tree<int, cmp::binary_tree_node<int>> tree; 

    std::mt19937 mt { std::random_device()() };
    std::uniform_int_distribution<int> dist { 0, 100 };

    tree_random_fill(tree, 2, [&]() { return dist(mt); });
    tree.print();

    assert(is_subtree(tree.root.get(), tree.root->left()));
    assert(is_subtree(tree.root.get(), tree.root->right()));

    return 0;
}
