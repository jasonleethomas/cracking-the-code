#pragma once

#include <map>
#include <memory>
#include <string>

namespace cmp {
    struct trie_node {
        trie_node(char data) :
            data(data),
            end(false)
        {}

        void append(char data) {
           children[data] = std::make_unique<trie_node>(data); 
        }
        
        char data;
        bool end;
        std::map<char, std::unique_ptr<trie_node>> children;
    };

    void trie_append_from(std::unique_ptr<trie_node> & node, std::string word) {
        if (word.empty()) {
            node->end = true;

            return;
        }

        if (node->children.find(word[0]) == std::end(node->children)) {
            node->append(word[0]); 
        }

        return trie_append_from(node->children[word[0]], word.substr(1));
    }

    bool trie_contains (trie_node * node, std::string word) {
        if (word.empty()) {
            if (node->end) {
                return true;
            }

            return false;
        }

        if (node->children.find(word[0]) == std::end(node->children)) {
            return false;
        }

        return trie_contains(node->children[word[0]].get(), word.substr(1));
    }
}
