#include "competitive.h"

char * reverse(char * str) {
    auto length { std::char_traits<char>::length(str) };
    std::stack<char> char_stack;
    for (auto i = 0; i < length; ++i) {
        char_stack.push(str[i]);
    }

    for (auto i = 0; i < length; ++i) {
        str[i] = char_stack.top();
        char_stack.pop();
    }

    return str;
}

int main(int argc, char ** argv) {
    std::cout << reverse(argv[1]) << std::endl;
    return 0;
}
