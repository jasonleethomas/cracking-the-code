#include "competitive.h"

namespace cmp {
    template<typename T, int PoolSize, int ArraySize>
    class shared_array_stack {
        static_assert(PoolSize < ArraySize, "ArraySize >= N");

    public:
        shared_array_stack(std::shared_ptr<std::array<T, ArraySize>> const & shared_array) :
            shared_array(shared_array),
            id(shared_count++),
            size(0)
        {
            assert (id < PoolSize);
        }


        int top_index() const {
            return size * PoolSize + id;
        }

        bool push(T data) {
            assert(!shared_array.expired());

            if (top_index() >= ArraySize) {
                return false;
            }

            auto stack = shared_array.lock();
            ++size;
            stack->at(top_index()) = data;

            return true;
        }

        T pop() {
            assert(!shared_array.expired());
            assert(size > 0);

            auto stack = shared_array.lock();
            auto top = stack->at(top_index());
            --size;

            return top;
        }


    private:
        std::weak_ptr<std::array<T, ArraySize>> shared_array;
        int const id;
        int size;

        static int shared_count;
    };

    template<typename T, int PoolSize, int ArraySize>
    int shared_array_stack<T, PoolSize, ArraySize>::shared_count = 0;
}

int main() { 
    int const MAX_SIZE = 100;
    int const STACK_SIZE = MAX_SIZE / 4;
    int const POOL_SIZE = 3;

    auto array = std::make_shared<std::array<int, MAX_SIZE>>();
    using stack_t = cmp::shared_array_stack<int, POOL_SIZE, MAX_SIZE>;
    auto stacks = std::array<stack_t, POOL_SIZE> {
        cmp::shared_array_stack<int, POOL_SIZE, MAX_SIZE>(array),
        cmp::shared_array_stack<int, POOL_SIZE, MAX_SIZE>(array),
        cmp::shared_array_stack<int, POOL_SIZE, MAX_SIZE>(array)
    };

    std::vector<int> data(STACK_SIZE);
    std::iota(std::begin(data), std::end(data), 0);


    std::for_each(std::begin(stacks), std::end(stacks), [data](auto & stack) {
        std::for_each(std::begin(data), std::end(data), 
                      [&stack](auto item) { assert(stack.push(item)); });

        std::for_each(std::rbegin(data), std::rend(data), 
                      [&stack](auto item) { assert(stack.pop() == item); });
    });

    return 0;
} 
