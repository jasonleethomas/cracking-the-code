#pragma once

#include <array>
#include <cassert>
#include <iostream>
#include <iterator>
#include <memory>
#include <random>
#include <type_traits>
#include <queue>
#include <vector>

namespace cmp {
    template<typename T>
    struct tree_node {
        tree_node(T data) :
            data(data)
        {}

        void append_child(T data) {
            children.emplace_back(new tree_node<T>(data));
        }

        T data;
        std::vector<std::unique_ptr<tree_node<T>>> children;
    };

    template<typename Node, typename F>
    void dfs_from(std::unique_ptr<Node> & node, F function) {
        if (!node) { return; }

        function(node.get());
        for (auto & child : node->children) {
            dfs_from(child, function);
        }
    }

    template<typename Node, typename F>
    void bfs_from(std::unique_ptr<Node> & node, F function) {
        if (!node) { return; }
        std::queue<Node *> nodes;
        function(node.get());
        nodes.push(node.get());
        while (!nodes.empty()) {
            auto front = nodes.front(); nodes.pop();
            function(front);
            for (auto & child : front->children) {
                nodes.push(child.get());
            }
        }
    }

    template<typename Node>
    int height_from_children(std::vector<std::unique_ptr<Node>> const & children, int const index) {
        if (index == 0) {
            return height_from(children[0].get());
        }

        return std::max(height_from(children[index].get()), height_from_children(children, index - 1));
    }

    template<typename Node>
    int height_from(Node const * node) {
        auto const size = node->children.size();
        if (size == 0) {
            return 0;
        }

        return 1 + height_from_children(node->children, size - 1);
    }

    template<typename Node, typename Generator>
    void fill_from(std::unique_ptr<Node> & node, int const order, Generator & generator) {
        if (!node) { return; }

        std::mt19937 mt { std::random_device()() };
        std::uniform_int_distribution<int> random_degree { 0, order };
        bfs_from(node, [&](auto & node) {
                    auto const degree = random_degree(mt);
                    std::vector<typename std::result_of<Generator()>::type> data(degree);
                    std::generate(std::begin(data), std::end(data), generator);
                    std::for_each(std::begin(data), std::end(data),
                                  [&node](auto & data) { node->append_child(data); });
                });
    }
}
