#include "competitive.h"

bool is_substring(std::string a, std::string b) {
    return a.find(b) != std::string::npos;
}

bool is_rotation(std::string a, std::string b) {
    return (a.size() == b.size()) && is_substring((a + a), b);
}

int main(int argc, char ** argv) {
    std::cout << (is_rotation(std::string(argv[1]), std::string(argv[2])) ?
                             "rotation" : "not rotation")
              << std::endl;
                             
    return 0;
}
