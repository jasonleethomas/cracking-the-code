#pragma once

#include "node.h"
#include "competitive.h"

namespace cmp {
    template<typename T, template<typename> class Node = node>
    struct Queue {
        std::shared_ptr<Node<T>> front, back;
        size_t count;

        void enqueue(T data) {
            auto node = std::make_shared<Node<T>>(data);
            if (front == back) {
                front = node;
                back = front;
            } else {
                back.next = node;
                back = back.next;
            }

            ++count;
        }

        auto dequeue() {
            assert(front != nullptr);

            auto data = front.data;
            front = front.next;
            
            --count;

            return data;
        }

        auto size() const {
            return count;
        }
    };
}
