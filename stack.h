#pragma once

#include <memory>

#include "node.h"

namespace cmp {
    template<typename T, template<typename> class Node = node>
    class stack {
    public:
        void push(T data) {
            auto node = std::make_shared<Node<T>>(data);
            if (top == nullptr) {
                top = node;
            } else {
                node.next = top;
                top = node;
            }
        }

        auto pop() {
            assert(top != nullptr);

            auto data = top.data;
            top = top.next;

            return data;
        }

        auto size() const {
            return count;
        }

    private:
        std::shared_ptr<Node<T>> top;
        size_t count;
    };
}
