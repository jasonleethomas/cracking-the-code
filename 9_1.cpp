#include "competitive.h"

std::vector<int> cache;

int hops(int stairs) {
    if (stairs <= 0)
        return 0;

    if (cache[stairs] != 0) {
        return cache[stairs];
    }

    cache[stairs] = 1 + hops(stairs - 1) + hops(stairs - 2) + hops(stairs - 3);

    return cache[stairs];
}

int main(int argc, char ** argv) {
    if (argc < 2) {
        std::cout << "usage: 4_8 stairs\n";
        return 1;
    }

    auto const stairs = static_cast<size_t>(std::stoi(argv[1]));
    cache = std::vector<int> (stairs + 1);

    std::cout << hops(stairs) << "\n";

    return 0;
}
