#include "competitive.h"
#include "linked_list.h"

template<typename T>
bool is_palindrome(cmp::linked_list<T> list) {
    if (list.size == 1) {
        return true;
    }

    auto const middle { list.size / 2 };
    auto const odd_size { list.size % 2 != 0 };  

    std::stack<T> data_stack;

    auto next { list.head };
    auto index { 0 };
    while (next != nullptr && index < list.size) {
        if (index < middle) {
            data_stack.push(next->data);
        } else {
            auto data = data_stack.top();
            data_stack.pop();

            if (data != next->data && 
                !(odd_size && index == middle)) {
                return false;
            }
        }

        next = next->next;
        ++index;
    }

    return true;
}

int main(int argc, char ** argv) {
    auto N { 0 };
    std::cin >> N;

    cmp::linked_list<int> list;
    for (auto index = 0; index < N; ++index) {
        auto n { 0 };
        std::cin >> n; 
        list.append(n);
    }

    std::cout << (is_palindrome(list) ? "palindrome\n" : "not palindrone\n");

    return 0;
}
