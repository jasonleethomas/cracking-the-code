#include "tree.h"
#include "competitive.h"

template<typename T, typename Node>
bool is_bst(Node node, T min, T max) {
    if (!node)
        return true;

    if (node->data < min || node->data > max)
        return false;

    return is_bst(node->left, min, node->data) && 
           is_bst(node->right, node->max, max);
}

int main() {
    return 0;
}
