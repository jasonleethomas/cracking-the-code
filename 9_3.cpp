#include "competitive.h"

int magic_index(std::vector<int> & A, int low, int high) {
    if (low > high) {
        return -1;
    }

    auto mid = (low + high) / 2;
    if (A[mid] == mid) {
        return mid;
    } else if (A[mid] > mid) {
        return magic_index(A, low, mid - 1);
    } else {
        return magic_index(A, mid + 1, high);
    }
}

int main() {
    std::vector<int> A {-1, 1, 3, 4, 5, 7, 8 };
    assert(magic_index(A, 0, A.size() - 1) == 1);

    return 0;
}
