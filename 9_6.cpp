#include "competitive.h"

namespace brace {
    auto const l = '(';
    auto const r = ')';
}

std::string cap (std::string left, std::string string) {
    std::for_each(std::begin(left), std::end(left),
                  [&string](auto b) { string.push_back(brace::r); });

    return string;
}
std::set<std::string>
brace_permutations (std::string braces) {
    if (braces.size() == 1) { 
        return std::set<std::string> {"()"}; 
    }

    std::set<std::string> sets;
    std::string left;
    while (!braces.empty()) {
        left.push_back(braces.back()); braces.pop_back();
        auto subsets = brace_permutations(braces);
        std::transform(std::begin(subsets), std::end(subsets),
                       std::inserter(sets, std::end(sets)),
                       [=](auto set) { return cap(left, left) + set; });
        
        std::transform(std::begin(subsets), std::end(subsets),
                       std::inserter(sets, std::end(sets)),
                       [=](auto set) { return cap(left, left + set); });

        std::transform(std::begin(subsets), std::end(subsets),
                       std::inserter(sets, std::end(sets)),
                       [=](auto set) { return set + cap(left, left); });
    }

    return sets;
}

int main(int argc, char ** argv) {
    if (argc < 2) {
        std::cout << "usage 9_6 N\n";

        return 1;
    }

    std::string braces;
    auto const no_pairs = std::stoi(argv[1]);

    for (auto p = 0; p < no_pairs; ++p) { braces.push_back(brace::l); }

    auto sets = brace_permutations(braces);
    std::copy(std::begin(sets), std::end(sets), 
              std::ostream_iterator<std::string>{std::cout, " "});
    std::cout << std::endl;
    return 0;
}
