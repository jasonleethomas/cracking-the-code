#pragma once

#include "node.h"

#include <cassert>
#include <memory>

namespace cmp {
    template<typename T, template<typename> class Node = node>
    struct linked_list {
        std::shared_ptr<Node<T>> head;
        std::shared_ptr<Node<T>> tail;

        linked_list() :
            head(nullptr),
            tail(nullptr),
            size(0)
        {}

        auto append(T data) {
            auto node = std::make_shared<Node<T>>(data);

            if (head == nullptr) {
                head = node; 
                tail = node;

                return head;
            }

            tail->next = node;
            tail = node;

            return node; 
        }

        auto remove(T data) {
            assert(head != nullptr);

            if (head->data == data) {
                head = head->next;

                return head;
            }

            auto next = head;
            while (next->next != tail) {
                if (next->next->data == data) {
                    next->next = next->next->next;    

                    return head;
                }

                next = next->next;
            }

            if (tail->data == data) {
                next->next.reset();
                tail = next;
            }

            return head;
        }
    };
}
