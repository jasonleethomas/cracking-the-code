#include "tree.h"
#include "competitive.h"

template<typename T, template<typename> class Node>
void tree_build(std::shared_ptr<Node<T>> & node, std::vector<T> & data, int left, int right) {
    if (left > right)
        return;

    auto middle = (left + right) / 2;
    node = std::make_shared<Node<T>>(data[middle]);

    node->children = std::vector<std::shared_ptr<Node<T>>>(2, nullptr);
    tree_build(node->children[0], data, left, middle - 1);
    tree_build(node->children[1], data, middle + 1, right);
}

int main() {
    int const size = 50;
    std::vector<int> data(size);
    std::iota(std::begin(data), std::end(data), 0);

    cmp::tree<int, cmp::binary_tree_node<int>> tree;

    tree_build(tree.root, data, 0, data.size() - 1);
    print_from(tree.root.get());

    return 0;
}
