#include "competitive.h"

using tower = std::stack<int>;

void push (tower & destination, int disk) {
    if (!destination.empty()) { assert(destination.top() > disk); }
    destination.push(disk);
}

void move_top(tower & origin, tower & destination) {
    assert(!origin.empty());
    push(destination, origin.top());
    origin.pop();
}

void move_disks(int disk, tower & origin, tower & destination, tower & buffer) {
    if (disk > 0) {
        move_disks(disk - 1, origin, buffer, destination);
        move_top(origin, destination);
        move_disks(disk - 1, buffer, destination, origin);
    }
}

int main() {
    tower left, center, right;

    int const no_disks = 6;
    std::vector<int> disks { no_disks };
    std::iota(std::begin(disks), std::end(disks), 0);

    std::for_each(std::rbegin(disks), std::rend(disks), 
                  [&left] (auto disk) {
                        push(left, disk);
                  }); 

    assert(!left.empty());

    move_disks(no_disks, left, right, center);

    assert(left.empty() && center.empty());
    assert(right.size() == no_disks);

    std::for_each(std::begin(disks), std::end(disks), 
                  [&right](auto disk) { 
                        auto top = right.top();
                        right.pop();
                        std::cout << disk << " " << top << "\n";
                  });

    return 0;
}
