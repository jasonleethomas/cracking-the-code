#include "graph.h"
#include "competitive.h"

template<typename I>
bool path_exists(I from, I to, cmp::adjacency_list<I> & adjacency_list) {
    bool found = false;

    std::map<I, bool> visited_map;
    auto visit = [&visited_map, &found, to](auto node) {
        visited_map[node.first] = true;
        if (node.first == to) {
            found = true; 
        }
    };

    auto visited = [&visited_map](auto node) {
        return visited_map[node.first];
    };

    cmp::bfs_from(std::make_pair(from, 0), adjacency_list, visit, visited);

    return found;
}

int main() {
    std::mt19937 mt { std::random_device {}() };

    std::uniform_int_distribution<int> vertex_dist { 50, 100 };
    auto const no_vertices { vertex_dist(mt) };

    std::vector<int> vertices(no_vertices);
    std::iota(std::begin(vertices), std::end(vertices), 0);

    std::uniform_int_distribution<int> no_edges_dist { 1, static_cast<int>(no_vertices * (no_vertices - 1) / 2) };
    auto const no_edges { no_edges_dist(mt) };

    std::uniform_int_distribution<int> index_dist { 0, static_cast<int>(vertices.size() - 1) };
    std::uniform_int_distribution<int> weight_dist { 1, 10 };

    std::vector<std::tuple<int, int, int>> edges (no_edges_dist(mt));
    std::generate(std::begin(edges), std::end(edges), 
                  [&]() { return std::make_tuple(index_dist(mt), index_dist(mt), weight_dist(mt)); });

    cmp::adjacency_list<int> adjacency_list;
    std::for_each(std::begin(edges), std::end(edges),
                  [&adjacency_list](auto const & edge) { 
                        adjacency_list[std::get<0>(edge)].emplace_back(std::get<1>(edge), std::get<2>(edge)); 
                  }); 

    std::for_each(std::begin(edges), std::end(edges),
                  [&adjacency_list](auto const & edge) { 
                        assert(path_exists(std::get<0>(edge), std::get<1>(edge), adjacency_list)); 
                  });
    return 0;
}
