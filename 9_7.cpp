#include "competitive.h"

template<typename T>
T clamp(T value, T lower, T upper) {
    return std::min(std::max(lower, value), upper);
}

template<typename Color>
void paint_screen(std::vector<std::vector<Color>> & screen,
                  std::pair<int, int> p,
                  Color from_color,
                  Color to_color) {

    screen[p.first][p.second] = to_color;

    auto const d = std::vector<int> {-1, 0, 1};
    for (auto const & dx : d) {
        for (auto const & dy : d) {
            auto const qx = p.first + dx;
            auto const qy = p.second + dy;
            auto const q = std::make_pair(clamp(qx, 0, screen.size()),
                                          clamp(qy, 0, screen.front().size()));

            if (screen[q.first][q.second] == from_color) {
                paint_screen(screen, q, from_color, to_color);
            }
        }
    }
}
