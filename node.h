#pragma once

#include <memory>

namespace cmp {
    template<typename T>
    struct node {
        node(T data) :
            data(data),
            next(nullptr)
        {}

        std::unique_ptr<node<T>> next;
        T data;
    };
}
