#include "competitive.h"

template<typename T>
void sort_stack(std::stack<T> & A) {
    std::stack<T> B;
    auto size_A { A.size() };
    for (auto i = 0; i < size_A; ++i) {
        auto min { A.top() };
        while (A.size() > i) {
            auto a = A.top();
            A.pop();

            if (a < min) {
                min = a;
            }

            B.push(a);
        }

        A.push(min);

        while (!B.empty()) {
            auto b = B.top();
            B.pop();

            if (b != min) {
                A.push(b);
            }
        }
    }
}

int main() {
    std::vector<int> B { 5, 4, 3, 6, 9, 2, 1, 0, 7, 10, -1, - 4 };

    std::sort(std::begin(B), std::end(B));

    std::stack<int>  A;
    for (auto b : B) { A.push(b); }

    sort_stack(A);

    assert(A.size() == B.size());

    std::for_each(std::rbegin(B), std::rend(B), 
                  [&A] (auto b) {
                        auto a = A.top(); A.pop();
                        assert(a == b);
                  });

    return 0;
}
