#include "competitive.h"
#include "linked_list.h"

auto remove_duplicates(cmp::linked_list<int> && list) {
    std::map<int, int> unique;

    auto next = list.head;
    while (next != nullptr) {
        auto data = next->data;

        ++unique[data];
        if (unique[data] > 1) {
            list.remove(data);
            list.print();
        }

        next = next->next;
    }

    list.print();
    return list;
}

int main(int argc, char ** argv) {
    std::vector<int> integers { 1, 1, 2, 2, 3, 4, 5, 6, 7, 7, 8 };

    cmp::linked_list<int> before;
    std::for_each(std::begin(integers), std::end(integers), 
                  [&list](auto i) { before.append(i); });

    std::cout << "before\n"; 
    before.print();

    auto after = remove_duplicates(before);

    std::cout << "\nafter\n"; 
    after.print();

    return 0;
}
