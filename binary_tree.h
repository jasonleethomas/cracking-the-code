#pragma once

#include <iterator>
#include <iostream>
#include <memory>
#include <queue>
#include <random>
#include <vector>

namespace cmp {
    template<typename T>
    struct binary_tree_node {
        binary_tree_node(T data) :
            data(data)
        {}

        void append_left(T data) {
            left = std::make_unique<binary_tree_node<T>>(data);
        }

        void append_right(T data) {
            right = std::make_unique<binary_tree_node<T>>(data);
        }

        T data;
        std::unique_ptr<binary_tree_node<T>> left, right;
    };

    template<typename T>
    int height_from(binary_tree_node<T> const * node) {
        if (!node) { return -1; }

        return 1 + std::max(height_from(node->left.get()), 
                            height_from(node->right.get()));
    }

    template<typename T>
    auto append_avl(std::unique_ptr<binary_tree_node<T>> & node, T data) {
        if (!node) {
            return std::make_unique<binary_tree_node<T>>(data);
        }

        if (node->data < data) {
            node->left = append_avl(node->left, data);
        }

        if (node->data >= data) {
            node->right = append_avl(node->right, data);
        }

        return balance(node);
    }

    template<typename T>
    auto rotate_right(std::unique_ptr<binary_tree_node<T>> & node) {
        auto temp { std::move(node->left) };
        node->left.swap(temp->right);
        temp->right.swap(node);

        return temp;
    }

    template<typename T>
    auto rotate_left(std::unique_ptr<binary_tree_node<T>> & node) {
        auto temp { std::move(node->right) };
        node->right.swap(temp->left);
        temp->left.swap(node);

        return temp;
    }

    template<typename T>
    auto balance_factor(binary_tree_node<T> const * node) {
        return height_from(node->left.get()) - 
               height_from(node->right.get()); 
    }

    template<typename T>
    auto balance(std::unique_ptr<binary_tree_node<T>> & node) {
        if (balance_factor(node.get()) > 1) {
            if (balance_factor(node->left.get()) < 0) {
                node->left = rotate_left(node->left);
            }

            return rotate_right(node);
        }

        if (balance_factor(node.get()) < -1) {
            if (balance_factor(node->right.get()) > 0) {
                node->right = rotate_right(node->right);
            }

            return rotate_left(node);
        }

        return std::move(node);
    }

    template<typename T, typename F>
    void bfs_from(std::unique_ptr<binary_tree_node<T>> & node, F function) {
        if (!node) { return; }
        std::queue<binary_tree_node<T> *> nodes;

        function(node.get());
        nodes.push(node.get());
        while (!nodes.empty()) {
            auto front = nodes.front(); nodes.pop();
            function(front);
            if (node->left)
                nodes.push(node->left.get());
            if (node->right)
                nodes.push(node->right.get());
        }
    }

    template<typename T, typename F>
    void dfs_from(std::unique_ptr<binary_tree_node<T>> & node, F function) {
        if (!node) { return; }

        function(node.get());
        dfs_from(node->left, function);
        dfs_from(node->right, function);
    }

    template<typename T, typename PairGenerator>
    void fill_from(std::unique_ptr<binary_tree_node<T>> & node, PairGenerator & generator) {
        if (!node) { return; }

        std::mt19937 mt { std::random_device{}() };
        std::uniform_int_distribution<int> random_degree { 0, 1 };

        auto data_pair = generator(node->data); 

        if (random_degree(mt)) {
            node->append_left(data_pair.first);
            fill_from(node->left, generator);
        }

        if (random_degree(mt)) {
            node->append_right(data_pair.second);
            fill_from(node->right, generator);
        }
    }

    template<typename T>
    void print_from(std::unique_ptr<binary_tree_node<T>> & node) {
        if (!node) { return; }
        std::queue<binary_tree_node<T> const *> nodes;
        nodes.push(node.get());
        auto parents = 1;
        while (!nodes.empty()) {
            auto children = 0;
            for (auto p = 0; p < parents; ++p) {
                auto front = nodes.front(); nodes.pop();
                std::cout << front->data << " "; 

                if (front->right) {
                    nodes.push(front->right.get());
                    ++children;
                }

                if (front->left) {
                    nodes.push(front->left.get());
                    ++children;
                }
            }

            std::cout << "\n";
            parents = children;
        }
    }
}
