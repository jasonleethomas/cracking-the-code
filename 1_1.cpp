#include "competitive.h"

bool unique(std::string input) {
    std::map<char, int> characters;
    std::for_each(std::begin(input), std::end(input),
                  [&characters](auto c) { ++characters[c]; });

    return all_of(std::begin(characters), std::end(characters),
                  [](auto cp) { return cp.second == 1; });
}

int main (int argc, char ** argv) {
    std::cout << (unique(std::string(argv[1])) ? "unique" : "not unique")
              << std::endl;
    return 0;
}
