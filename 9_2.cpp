#include "competitive.h"

std::vector<std::vector<int>> cache;

int no_paths(int x, int y, int X, int Y) {
    if (x == X && y == Y)
        return 0;

    if (x == X || y == Y)
        return 1;

    if (cache[x][y] != 0)
        return cache[x][y];

    cache[x][y] = no_paths(x + 1, y, X, Y) + no_paths(x, y + 1, X, Y);

    return cache[x][y];
}

int main(int argc, char ** argv) {
    if (argc < 3) {
        std::cout << "usage 9_2 X Y\n";
        return 1;
    }
    
    auto const X = std::stoi(argv[1]);
    auto const Y = std::stoi(argv[2]);

    assert(X > 0 && Y > 0);

    cache = std::vector<std::vector<int>>(X + 1, std::vector<int>(Y + 1, 0));

    std::cout << no_paths(0, 0, X, Y) << std::endl;

    return 0;
}
