#include "competitive.h"

template<typename RandomIter>
void insertion_sort(RandomIter first, RandomIter last) {
    auto begin = first;
    for (; first != last; ++first) {
        auto pivot = first;
        while (pivot > begin && *first < *(first - 1)) {
            std::swap(*first, *(first - 1));
            --pivot;
        }
    }
}

template<typename RandomIter>
RandomIter 
partition(RandomIter first, RandomIter last) {
    auto pivot = *first;
    for (; first != last; ++first) {
        if (*first <= *last) {
            std::swap(*first, *pivot);
            ++pivot;
        }
    }

    std::swap(*last, *pivot);
    return pivot;
}

template<typename RandomIter>
void quick_sort(RandomIter first, RandomIter last) {
    if (first < last) {
        auto pivot = partition(first, last);
        quick_sort(first, last - 1);
        quick_sort(pivot + 1, last);
    }
}

template<typename RandomIter>
void merge(RandomIter first, RandomIter middle, RandomIter last) {
    auto f = first;
    auto m = middle + 1;
}

template<typename RandomIter>
void merge_sort(RandomIter first, RandomIter last) {
    if (first < last) {
        auto mid = first + (last - first) / 2;
        merge_sort(first, mid);
        merge_sort(mid + 1, last);
        merge(first, mid, last);
    }
}
