#include <algorithm>
#include <vector>

template<typename T>
class heap_array {
public:
    T peek() const {
        assert (!table.empty());
        return *std::begin(table);
    }

    T pop() {
        std::swap(table[0], table[table.size() - 1]);
        auto value = table.back();
        table.pop_back();
        bubble_down(0);

        return value;
    }

    void push(T item) {
        table.push_back(item);
        bubble_up(table.size() - 1);
    }

    size_t size() const {
        return table.size();
    }

private:
    int parent_of(int index) const {
        auto parent = index / 2;
        return index % 2 == 0? parent - 1: parent; 
    }

    int left_of(int index) const {
        return 2 * index + 1;
    }

    int right_of(int index) const {
        return 2 * index + 2;
    }

    void bubble_up(int index) {
        auto const parent = parent_of(index);
        if (!in_table(index) || !in_table(parent)) {
            return;
        }

        if (table[parent] < table[index]) {
            std::swap(table[parent], table[index]);
            bubble_up(parent);
        }
    }

    void bubble_down(int index) {
        if (!in_table(index)) {
            return;
        }

        auto const left = left_of(index);
        auto const right = right_of(index);

        auto child = -1;
        if (in_table(left) && in_table(right)) {
            child = table[left] > table[right]? left : right;
        } else if (in_table(left)) {
            child = left;
        } else if (in_table(right)) {
            child = right;
        } else {
            return;
        }

        if (table[child] > table[index]) {
            std::swap(table[child], table[index]);
            bubble_down(child);
        }
    }

    bool in_table(int index) {
        return index > -1 && index < table.size();
    }

    std::vector<T> table;
};


