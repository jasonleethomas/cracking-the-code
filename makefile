%:
	g++ -g -O0 $@.cpp -std=c++1z -o $@

clean:
	find . -name "*.sw*" -delete
	rm -r -f *.dSYM*
	rm -f *.gch*
