#include "tree.h"
#include "competitive.h"

template<typename T, template<typename> Node, template<typename> class Tree>
void get_depth_list(Tree & tree, std::vector<std::vector<T>> & list) {
    std::queue<Node<T>> queue;

    list.push_back(std::vector<T> { tree.root->data });
    auto parents = 1, children = 0;

    queue.push(tree.root);
    while (!queue.empty()) {
        std::vector<T> depth;
        for (auto p = 0; p < parents; ++p) {
            auto next = queue.front(); queue.pop();
            for (auto & child : next->children) {
                if (child) {
                    ++children;
                    depth.push_back(child->data);
                }
            }
        }

        list.push_back(depth);
        parents = children;
        children = 0;
    }
}

int main() {

    return 0;
}
