#include "competitive.h"

std::set<std::string> 
all_string_permutations(std::string s) {
    if (s.empty()) { return std::set<std::string> {""}; }

    auto prefix { std::string { s.front() } };
    auto remainder { s.substr(1) };
    auto sub_permutations { all_string_permutations(remainder) };

    auto insert_copy = [](auto string, auto index, auto input) {
        return string.insert(index, input);
    };

    std::set<std::string> permutations;

    for (auto p : sub_permutations) {
        if (p.empty()) {
            permutations.insert(prefix); 
        } else {
            for (auto index = 0; index < p.size(); ++index) {
                permutations.insert(insert_copy(p, index, prefix));
            }
        }
    }

    return permutations;
}

int main(int argc, char ** argv) {
    if (argc < 2) {
        std::cout << "usage 9_5 string\n";

        return 1;
    }

    auto string { argv[1] };
    auto permutations { all_string_permutations(string) };
    std::copy(std::begin(permutations), std::end(permutations),
              std::ostream_iterator<std::string>(std::cout, " "));

    return 0;
}
