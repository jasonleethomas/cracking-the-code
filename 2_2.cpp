#include "competitive.h"
#include "linked_list.h"

template<typename T>
T kth_item(cmp::linked_list<T> list, int k) {
    auto next = list.head;
    auto prev = next;

    int size = 0;
    while (next != nullptr) {
        next = next->next;
        ++size;
    }

    for (int i = 1; i < size - k; ++i) {
        prev = prev->next;
    }

    std::cout << size << std::endl;
    return prev->data;
}

int main() {
    std::vector<int> data {1, 2, 3, 4, 5};

    cmp::linked_list<int> list;
    std::for_each(std::begin(data), std::end(data),
                  [&list](int i) {
                       list.append(i);
                  });

    auto second = kth_item(list, 2);
    std::cout << second << std::endl;
    assert(second == 3);

    return 0;
}
